export interface ContinentModel {
    _id?: string;
    name: string;
    newName: string;
    edit?: boolean;
}
