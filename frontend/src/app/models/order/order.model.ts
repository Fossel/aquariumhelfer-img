export interface OrderModel {
    _id?: string;
    nameDe: string;
    nameLt: string;
    newNameLt: string;
    newNameDe: string;
    edit?: boolean;
}
