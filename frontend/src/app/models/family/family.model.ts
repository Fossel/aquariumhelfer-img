export interface FamilyModel {
    _id?: string;
    nameDe: string;
    nameLt: string;
    newNameLt: string;
    newNameDe: string;
    edit?: boolean;
}
