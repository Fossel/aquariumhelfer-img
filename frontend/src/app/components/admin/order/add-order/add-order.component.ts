import { Component, OnInit } from '@angular/core';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { OrderService } from '../../../../services/order/order.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-add-order',
    templateUrl: './add-order.component.html',
    styleUrls: ['./add-order.component.scss']
})
export class AddOrderComponent implements OnInit {
    public orderForm: FormGroup;
    public nameLt: FormControl = new FormControl('', [Validators.required]);
    public nameDe: FormControl = new FormControl('');

    public nameLtConfig: InputInterface = {
        label: 'Name lateinisch',
        type: 'text',
        validation: false,
        validationText: 'Bitte geben Sie einen lateinischen Namen ein'
    };

    public nameDeConfig: InputInterface = {
        label: 'Name deutsch',
        type: 'text',
        validation: false,
        validationText: ''
    };

    constructor(
        private formBuilder: FormBuilder,
        private orderService: OrderService
    ) {
    }

    ngOnInit() {
        this.orderForm = this.formBuilder.group({
            nameLt: this.nameLt,
            nameDe: this.nameDe
        });
    }

    submit() {
        this.orderService.add(this.orderForm.get('nameLt').value, this.orderForm.get('nameDe').value);
        this.orderForm.reset();
    }
}
