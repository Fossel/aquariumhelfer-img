import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { map } from 'rxjs/operators';
import { OrderModel } from '../../../../models/order/order.model';
import { OrderService } from '../../../../services/order/order.service';

@Component({
    selector: 'app-edit-order',
    templateUrl: './edit-order.component.html',
    styleUrls: ['./edit-order.component.scss']
})
export class EditOrderComponent implements OnInit {

    public orders: OrderModel[] = [];
    public isLoading = false;
    private orderSub: Subscription;

    public notificationData = {
        text: 'Es wurden noch keine Ordnung angelegt',
        status: 'info',
        show: true
    };
    public nameLtConfig: InputInterface = {
        label: 'Name lateinisch',
        type: 'text',
        validation: false,
        validationText: 'Bitte geben Sie einen lateinischen Namen ein'
    };
    public nameDeConfig: InputInterface = {
        label: 'Name deutsch',
        type: 'text',
        validation: false,
        validationText: ''
    };

    constructor(
        private orderService: OrderService
    ) {
    }

    ngOnInit() {
        this.isLoading = true;
        this.orderService.getAll();
        this.orderSub = this.orderService.getOrderUpdateListener().pipe(
            map(orders => {
                for (const order of orders) {
                    order.edit = false;
                    order.newNameDe = order.nameDe;
                    order.newNameLt = order.nameLt;
                }
                return orders;
            })
        ).subscribe((getData) => {
            this.isLoading = false;
            this.orders = getData;
        });
    }

    update(item) {
        this.orderService.update({
            id: item._id,
            nameLt: item.newNameLt,
            nameDe: item.newNameDe
        });
    }

    delete(id) {
        this.orderService.delete(id);
    }
}
