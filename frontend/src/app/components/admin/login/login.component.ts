import { Component, OnInit } from '@angular/core';
import { InputInterface } from '../../global/forms/input/input-interface';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/admin/auth/auth.service';

@Component ({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public loginForm: FormGroup;
    public email: FormControl = new FormControl ('', [Validators.required, Validators.email]);
    public password: FormControl = new FormControl ('', [Validators.required]);
    public notifcationData = {
        text: 'Login ungültig',
        status: 'error',
        show: false
    };
    public inputEmailConfig: InputInterface = {
        label: 'Email',
        type: 'email',
        validation: true,
        validationText: 'Bitte geben Sie eine gültige E-Mail ein',
    };

    public inputPasswordConfig: InputInterface = {
        label: 'Passwort',
        type: 'password',
        validation: false,
    };

    constructor (
        private formBuilder: FormBuilder,
        private router: Router,
        private authService: AuthService
    ) {
    }

    ngOnInit() {
        if(this.authService.isLoggedIn()) {
            this.router.navigate(['admin/dashboard']);
        }
        this.loginForm = this.formBuilder.group ({
            email: this.email,
            password: this.password
        });
    }

    login() {
        if (!this.authService.login (this.email.value, this.password.value)) {
            this.notifcationData.show = true;
        }
        // if (this.email.value === 'mircosteinau@web.de' && this.password.value === '1234') {
        //      console.log('Login');
        //     this.router.navigate(['admin/dashboard']);
        // } else {
        // }
    }
}
