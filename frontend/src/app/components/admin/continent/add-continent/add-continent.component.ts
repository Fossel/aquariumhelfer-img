import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { ContinentService } from '../../../../services/continent/continent.service';

@Component({
  selector: 'app-add-continent',
  templateUrl: './add-continent.component.html',
  styleUrls: ['./add-continent.component.scss']
})
export class AddContinentComponent implements OnInit {
  public continentForm: FormGroup;
  public continentName: FormControl = new FormControl('', [Validators.required]);

  public continentNameConfig: InputInterface = {
    label: 'Kontinent',
    type: 'text',
    validation: false,
    validationText: 'Bitte geben Sie einen Kontinent Namen ein'
  };

  constructor(
      private formBuilder: FormBuilder,
      private continentService: ContinentService
  ) {
  }

  ngOnInit() {
    this.continentForm = this.formBuilder.group({
      continent: this.continentName
    });
  }

  submit() {
    this.continentService.add(this.continentForm.get('continent').value);
    this.continentForm.reset();
  }
}
