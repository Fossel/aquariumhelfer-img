import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { map } from 'rxjs/operators';
import { ContinentService } from '../../../../services/continent/continent.service';
import { ContinentModel } from '../../../../models/continent/continent.model';

@Component({
  selector: 'app-edit-continent',
  templateUrl: './edit-continent.component.html',
  styleUrls: ['./edit-continent.component.scss']
})
export class EditContinentComponent implements OnInit {
  public continents: ContinentModel[] = [];
  public isLoading = false;
  private continentSub: Subscription;

  public notificationData = {
    text: 'Es wurden noch keine Kontinent angelegt',
    status: 'info',
    show: true
  };
  public continentNameConfig: InputInterface = {
    label: 'Name',
    type: 'text',
    validation: false,
    validationText: 'Bitte geben Sie einen Kontinenten Namen ein'
  };

  constructor(
      private continentService: ContinentService
  ) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.continentService.getAll();
    this.continentSub = this.continentService.getContinentUpdateListener().pipe(
        map(continents => {
          for (const continent of continents) {
            continent.edit = false;
            continent.newName = continent.name;
          }
          return continents;
        })
    ).subscribe((getData) => {
      this.isLoading = false;
      this.continents = getData;
    });
  }

  update(item) {
    this.continentService.update({
      id: item._id,
      name: item.newName
    });
  }

  delete(id) {
    this.continentService.del(id);
  }
}
