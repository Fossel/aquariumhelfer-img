import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { map } from 'rxjs/operators';
import { FamilyService } from '../../../../services/family/family.service';
import { FamilyModel } from '../../../../models/family/family.model';

@Component({
  selector: 'app-edit-family',
  templateUrl: './edit-family.component.html',
  styleUrls: ['./edit-family.component.scss']
})
export class EditFamilyComponent implements OnInit {
  public families: FamilyModel[] = [];
  public isLoading = false;
  private familySub: Subscription;

  public notificationData = {
    text: 'Es wurden noch keine Familie angelegt',
    status: 'info',
    show: true
  };
  public nameLtConfig: InputInterface = {
    label: 'Name lateinisch',
    type: 'text',
    validation: false,
    validationText: 'Bitte geben Sie einen lateinischen Namen ein'
  };
  public nameDeConfig: InputInterface = {
    label: 'Name deutsch',
    type: 'text',
    validation: false,
    validationText: ''
  };

  constructor(
      private familyService: FamilyService
  ) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.familyService.getAll();
    this.familySub = this.familyService.getFamilyUpdateListener().pipe(
        map(families => {
          for (const family of families) {
            family.edit = false;
            family.newNameDe = family.nameDe;
            family.newNameLt = family.nameLt;
          }
          return families;
        })
    ).subscribe((getData) => {
      this.isLoading = false;
      this.families = getData;
    });
  }

  update(item) {
    this.familyService.update({
      id: item._id,
      nameLt: item.newNameLt,
      nameDe: item.newNameDe
    });
  }

  delete(id) {
    this.familyService.delete(id);
  }
}
