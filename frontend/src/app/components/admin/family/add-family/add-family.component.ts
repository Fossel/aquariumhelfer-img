import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { FamilyService } from '../../../../services/family/family.service';

@Component({
  selector: 'app-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public nameLt: FormControl = new FormControl('', [Validators.required]);
  public nameDe: FormControl = new FormControl('');

  public nameLtConfig: InputInterface = {
    label: 'Name lateinisch',
    type: 'text',
    validation: false,
    validationText: 'Bitte geben Sie einen lateinischen Namen ein'
  };
  public nameDeConfig: InputInterface = {
    label: 'Name deutsch',
    type: 'text',
    validation: false,
    validationText: ''
  };

  constructor(
      private formBuilder: FormBuilder,
      private familyService: FamilyService
  ) {
  }

  ngOnInit() {
    this.familyForm = this.formBuilder.group({
      nameLt: this.nameLt,
      nameDe: this.nameDe
    });
  }

  submit() {
    this.familyService.add(this.familyForm.get('nameLt').value, this.familyForm.get('nameDe').value);
    this.familyForm.reset();
  }
}
