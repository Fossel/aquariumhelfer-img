import { Component, HostListener, Inject, OnInit, Renderer2 } from '@angular/core';
import { AuthService } from '../../../services/admin/auth/auth.service';
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'app-admin-navigation',
    templateUrl: './admin-navigation.component.html'
})

export class AdminNavigationComponent implements OnInit {
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth > 1024) {
            this.closeNavigation();
            this.closeDropdowns();
        }
    }

    public toggler: boolean = false;
    public dropdown = {
        'fish': false,
        'tribus': false,
        'myAccount': false
    };

    constructor(
        private authService: AuthService,
        private renderer: Renderer2,
        @Inject(DOCUMENT) private document: Document
    ) {
    }

    ngOnInit() {
    }

    toggleNav() {
        if (this.toggler) {
            this.closeNavigation();
        } else {
            this.renderer.addClass(this.document.body, 'navigation-active');
            this.toggler = true;
        }
    }

    toggleDropdown(item: string) {
        this.dropdown[item] = !this.dropdown[item];
    }

    closeDropdowns() {
        for (const dropdown of Object.keys(this.dropdown)) {
            this.dropdown[dropdown] = false;
        }
        this.closeNavigation();
    }

    closeNavigation() {
        this.toggler = false;
        this.renderer.removeClass(this.document.body, 'navigation-active');
    }

    logout() {
        this.authService.logout();
        this.closeNavigation();
    }
}
