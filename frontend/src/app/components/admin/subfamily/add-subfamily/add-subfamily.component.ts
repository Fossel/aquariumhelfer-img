import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { SubfamilyService } from '../../../../services/subfamily/subfamily.service';

@Component({
  selector: 'app-add-subfamily',
  templateUrl: './add-subfamily.component.html',
  styleUrls: ['./add-subfamily.component.scss']
})
export class AddSubfamilyComponent implements OnInit {
  public subFamilyForm: FormGroup;
  public nameLt: FormControl = new FormControl('', [Validators.required]);
  public nameDe: FormControl = new FormControl('');

  public nameLtConfig: InputInterface = {
    label: 'Name lateinisch',
    type: 'text',
    validation: false,
    validationText: 'Bitte geben Sie einen lateinischen Namen ein'
  };

  public nameDeConfig: InputInterface = {
    label: 'Name deutsch',
    type: 'text',
    validation: false,
    validationText: ''
  };

  constructor(
      private formBuilder: FormBuilder,
      private subFamilyService: SubfamilyService
  ) {
  }

  ngOnInit() {
    this.subFamilyForm = this.formBuilder.group({
      nameLt: this.nameLt,
      nameDe: this.nameDe
    });
  }

  submit() {
    this.subFamilyService.add(this.subFamilyForm.get('nameLt').value, this.subFamilyForm.get('nameDe').value);
    this.subFamilyForm.reset();
  }
}
