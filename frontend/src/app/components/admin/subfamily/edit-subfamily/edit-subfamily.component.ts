import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { map } from 'rxjs/operators';
import { SubfamilyModel } from '../../../../models/subfamily/subfamily.model';
import { SubfamilyService } from '../../../../services/subfamily/subfamily.service';

@Component({
  selector: 'app-edit-subfamily',
  templateUrl: './edit-subfamily.component.html',
  styleUrls: ['./edit-subfamily.component.scss']
})
export class EditSubfamilyComponent implements OnInit {

  public subfamilies: SubfamilyModel[] = [];
  public isLoading = false;
  private subFamilySub: Subscription;

  public notificationData = {
    text: 'Es wurden noch keine Unterfamilie angelegt',
    status: 'info',
    show: true
  };
  public nameLtConfig: InputInterface = {
    label: 'Name lateinisch',
    type: 'text',
    validation: false,
    validationText: 'Bitte geben Sie einen lateinischen Namen ein'
  };
  public nameDeConfig: InputInterface = {
    label: 'Name deutsch',
    type: 'text',
    validation: false,
    validationText: ''
  };

  constructor(
      private subfamilyService: SubfamilyService
  ) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.subfamilyService.getAll();
    this.subFamilySub = this.subfamilyService.getSubfamilyUpdateListener().pipe(
        map(subFamilies => {
          for (const subFamily of subFamilies) {
            subFamily.edit = false;
            subFamily.newNameDe = subFamily.nameDe;
            subFamily.newNameLt = subFamily.nameLt;
        }
          return subFamilies;
        })
    ).subscribe((underFamilies) => {
      this.isLoading = false;
      this.subfamilies = underFamilies;
    });
  }

  update(item) {
    this.subfamilyService.update({
      id: item._id,
      nameLt: item.newNameLt,
      nameDe: item.newNameDe
    });
  }

  delete(id) {
    this.subfamilyService.delete(id);
  }
}
