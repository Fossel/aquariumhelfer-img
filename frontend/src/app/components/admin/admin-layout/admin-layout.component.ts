import { AfterContentInit, Component, OnInit } from '@angular/core';
import { ContinentService } from '../../../services/continent/continent.service';
import { TribusService } from '../../../services/tribus/tribus.service';
import { FamilyService } from '../../../services/family/family.service';
import { SubfamilyService } from '../../../services/subfamily/subfamily.service';
import { OrderService } from '../../../services/order/order.service';
import { SuborderService } from '../../../services/suborder/suborder.service';
import { OnlineOfflineServiceService } from '../../../services/onlineOfflineService/online-offline-service.service';
import { IndexDBService } from '../../../services/indexDB/index-db.service';

@Component({
    selector: 'app-admin-layout',
    templateUrl: './admin-layout.component.html',
    styleUrls: ['./admin-layout.component.scss'],
})
export class AdminLayoutComponent implements OnInit {

    constructor(
        private continentService: ContinentService,
        private tribusService: TribusService,
        private familyService: FamilyService,
        private subfamilyService: SubfamilyService,
        private orderService: OrderService,
        private underOrderService: SuborderService,
        private onlineOfflineStatus: OnlineOfflineServiceService,
        private indexDbService: IndexDBService
    ) {
    }

    ngOnInit() {
        this.cacheAdminApis();
        this.indexDbService.createDatabase();
        this.detectOnlineStatus();
    }

    public detectOnlineStatus() {
        this.onlineOfflineStatus.connectionChanged.subscribe(online => {
            if (online) {
                this.updateApis();
            }
        });
    }

    cacheAdminApis() {
        this.continentService.getAll();
        this.tribusService.getAll();
        this.familyService.getAll();
        this.subfamilyService.getAll();
        this.orderService.getAll();
        this.underOrderService.getAll();
    }

    updateApis() {
        this.tribusService.updateTribus();
        this.familyService.updateFamily();
        this.subfamilyService.updateSubfamily();
        this.orderService.updateOrder();
        this.continentService.updateContinent();
    }
}
