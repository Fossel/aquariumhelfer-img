import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TribusService } from '../../../../services/tribus/tribus.service';
import { TribusModel } from '../../../../models/tribus/tribus.model';
import { map } from 'rxjs/operators';
import { InputInterface } from '../../../global/forms/input/input-interface';

@Component({
    selector: 'app-delete-tribus',
    templateUrl: './edit-tribus.component.html',
    styleUrls: ['./edit-tribus.component.scss']
})
export class EditTribusComponent implements OnInit {
    public tribus: TribusModel[] = [];
    public isLoading = false;
    private tribusSub: Subscription;

    public notificationData = {
        text: 'Es wurden noch keine Tribus angelegt',
        status: 'info',
        show: true
    };
    public nameLtConfig: InputInterface = {
        label: 'Name lateinisch',
        type: 'text',
        validation: false,
        validationText: 'Bitte geben Sie einen lateinischen Namen ein'
    };
    public nameDeConfig: InputInterface = {
        label: 'Name deutsch',
        type: 'text',
        validation: false,
        validationText: ''
    };

    constructor(
        private tribusService: TribusService
    ) {
    }

    ngOnInit() {
        this.isLoading = true;
        this.tribusService.getAll();
        this.tribusSub = this.tribusService.getTribusUpdateListener().pipe(
            map(tribus => {
                for (const tribusItem of tribus) {
                    tribusItem.edit = false;
                    tribusItem.newNameDe = tribusItem.nameDe;
                    tribusItem.newNameLt = tribusItem.nameLt;
                }
                return tribus;
            })
        ).subscribe((getData) => {
            this.isLoading = false;
            this.tribus = getData;
        });
    }

    update(item) {
        this.tribusService.update({
            id: item._id,
            nameLt: item.newNameLt,
            nameDe: item.newNameDe
        });
    }

    delete(id) {
        this.tribusService.del(id);
    }
}
