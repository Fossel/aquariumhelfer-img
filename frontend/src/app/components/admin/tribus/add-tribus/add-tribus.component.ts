import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { TribusService } from '../../../../services/tribus/tribus.service';

@Component({
    selector: 'app-add-tribus',
    templateUrl: './add-tribus.component.html',
    styleUrls: ['./add-tribus.component.scss']
})
export class AddTribusComponent implements OnInit {
    public tribusForm: FormGroup;
    public nameLt: FormControl = new FormControl('', [Validators.required]);
    public nameDe: FormControl = new FormControl('');

    public nameLtConfig: InputInterface = {
        label: 'Name lateinisch',
        type: 'text',
        validation: false,
        validationText: 'Bitte geben Sie einen lateinischen Namen ein'
    };

    public nameDeConfig: InputInterface = {
        label: 'Name deutsch',
        type: 'text',
        validation: false,
        validationText: ''
    };

    constructor(
        private formBuilder: FormBuilder,
        private tribusService: TribusService
    ) {
    }

    ngOnInit() {
        this.tribusForm = this.formBuilder.group({
            nameLt: this.nameLt,
            nameDe: this.nameDe
        });
    }

    submit() {
        this.tribusService.add(this.tribusForm.get('nameLt').value, this.tribusForm.get('nameDe').value);
        this.tribusForm.reset();
    }
}
