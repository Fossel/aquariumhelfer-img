import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { SuborderService } from '../../../../services/suborder/suborder.service';

@Component({
  selector: 'app-add-suborder',
  templateUrl: './add-suborder.component.html',
  styleUrls: ['./add-suborder.component.scss']
})
export class AddSuborderComponent implements OnInit {
  public suborderForm: FormGroup;
  public nameLt: FormControl = new FormControl('', [Validators.required]);
  public nameDe: FormControl = new FormControl('');

  public nameLtConfig: InputInterface = {
    label: 'Name lateinisch',
    type: 'text',
    validation: false,
    validationText: 'Bitte geben Sie einen lateinischen Namen ein'
  };

  public nameDeConfig: InputInterface = {
    label: 'Name deutsch',
    type: 'text',
    validation: false,
    validationText: ''
  };

  constructor(
      private formBuilder: FormBuilder,
      private suborderService: SuborderService
  ) {
  }

  ngOnInit() {
    this.suborderForm = this.formBuilder.group({
      nameLt: this.nameLt,
      nameDe: this.nameDe
    });
  }

  submit() {
    this.suborderService.add(this.suborderForm.get('nameLt').value, this.suborderForm.get('nameDe').value);
    this.suborderForm.reset();
  }
}
