import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { InputInterface } from '../../../global/forms/input/input-interface';
import { map } from 'rxjs/operators';
import { SuborderModel } from '../../../../models/suborder/suborder.model';
import { SuborderService } from '../../../../services/suborder/suborder.service';

@Component({
    selector: 'app-edit-suborder',
    templateUrl: './edit-suborder.component.html',
    styleUrls: ['./edit-suborder.component.scss']
})
export class EditSuborderComponent implements OnInit {
    public suborders: SuborderModel[] = [];
    public isLoading = false;
    private suborderSub: Subscription;

    public notificationData = {
        text: 'Es wurden noch keine Unterordnungen angelegt',
        status: 'info',
        show: true
    };
    public nameLtConfig: InputInterface = {
        label: 'Name lateinisch',
        type: 'text',
        validation: false,
        validationText: 'Bitte geben Sie einen lateinischen Namen ein'
    };
    public nameDeConfig: InputInterface = {
        label: 'Name deutsch',
        type: 'text',
        validation: false,
        validationText: ''
    };

    constructor(
        private suborderService: SuborderService
    ) {
    }

    ngOnInit() {
        this.isLoading = true;
        this.suborderService.getAll();
        this.suborderSub = this.suborderService.getSuborderUpdateListener().pipe(
            map(suborders => {
                for (const suborder of suborders) {
                    suborder.edit = false;
                    suborder.newNameDe = suborder.nameDe;
                    suborder.newNameLt = suborder.nameLt;
                }
                return suborders;
            })
        ).subscribe((getData) => {
            this.isLoading = false;
            this.suborders = getData;
        });
    }

    update(item) {
        this.suborderService.update({
            id: item._id,
            nameLt: item.newNameLt,
            nameDe: item.newNameDe
        });
    }
    delete(id) {
        this.suborderService.delete(id);
    }
}
