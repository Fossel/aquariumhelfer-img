import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnChanges {
  @Input() cardData;
  public img = null;
  private imgFolder = '/assets/images/fische/';

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.cardData !== undefined) {
      if(this.cardData.previewImage) {

        this.img = this.imgFolder+this.cardData.previewImage;
      } else {
        this.img = this.imgFolder+'placeholder.jpg';
      }
    }
  }

  ngOnInit() {
    this.img = this.imgFolder+this.cardData.previewImage;
  }

  getImage(img) {
    if(img) {
      img = this.imgFolder+this.cardData.previewImage;
    } else {
      img = this.imgFolder+'placeholder.jpg';
    }
    return img
  }
}
