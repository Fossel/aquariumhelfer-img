export interface InputInterface {
    label?: string;
    type: string;
    validationText?: string;
    validation: boolean;
}
