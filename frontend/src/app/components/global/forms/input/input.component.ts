import { Component, Input, forwardRef, OnChanges, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { InputInterface } from './input-interface';

@Component({
    selector: 'app-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => InputComponent),
        multi: true
    }]
})
export class InputComponent implements ControlValueAccessor, OnChanges {

    @Input() valid: boolean;
    @Input() pristine: string = '';
    @Input() config: InputInterface;
    public showError: boolean = false;
    public timeout = null;

    constructor(
        private element: ElementRef
    ) {
    }

    @Input('inputValue') private _inputValue = '';

    get inputValue() {
        return this._inputValue;
    }

    set inputValue(val) {
        if (val && val.length > 0) {
            this.element.nativeElement.querySelector('input').classList.add('filled');
        } else {
            this.element.nativeElement.querySelector('input').classList.remove('filled');
        }

        if (this.config.validation) {
            this.showError = false;

            clearTimeout(this.timeout);

            this.timeout = setTimeout(() => {
                this.validateInput();
            }, 200);
        }

        this._inputValue = val;
        this.propagateChange(val);
    }

    propagateChange: any = () => {
    };

    ngOnChanges(inputs) {
        if (inputs.counterRangeMax || inputs.counterRangeMin) {
            this.propagateChange(this.inputValue);
        }
    }

    writeValue(value) {
        this.inputValue = value;
    }

    registerOnChange(fn) {
        this.showError = false;
        this.propagateChange = fn;
    }

    validateInput() {
        if (this.pristine === 'false') {
            this.showError = true;
        }
    }

    registerOnTouched() {
    }
}
