export interface SelectInterface {
    label: string;
    options: SelectOptionInterface[];
}
export interface SelectOptionInterface {
    text: string;
    value: string;
    selected?: boolean;
}