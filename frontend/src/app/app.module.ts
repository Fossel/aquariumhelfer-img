import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { AddFishComponent } from './pages/admin/fish/add-fish/add-fish.component';
import { DeleteFishComponent } from './pages/admin/fish/delete-fish/delete-fish.component';
import { EditFishComponent } from './pages/admin/fish/edit-fish/edit-fish.component';
import { AllFishesComponent } from './pages/public/fishes/all-fishes/all-fishes.component';
import { LoginComponent } from './components/admin/login/login.component';
import { AdminLayoutComponent } from './components/admin/admin-layout/admin-layout.component';
import { AdminNavigationComponent } from './components/admin/admin-navigation/admin-navigation.component';
import { InputComponent } from './components/global/forms/input/input.component';
import { CardComponent } from './components/global/card/card.component';
import { SelectComponent } from './components/global/forms/select/select.component';
import { FormsModule } from '@angular/forms';
import { NotificationComponent } from './components/global/notification/notification.component';
import { HttpClientModule } from '@angular/common/http';
import { TribusComponent } from './pages/admin/tribus/tribus.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AddFamilyComponent } from './components/admin/family/add-family/add-family.component';
import { EditFamilyComponent } from './components/admin/family/edit-family/edit-family.component';
import { AddOrderComponent } from './components/admin/order/add-order/add-order.component';
import { EditOrderComponent } from './components/admin/order/edit-order/edit-order.component';
import { EditSuborderComponent } from './components/admin/suborder/edit-suborder/edit-suborder.component';
import { AddSuborderComponent } from './components/admin/suborder/add-suborder/add-suborder.component';
import { AddSubfamilyComponent } from './components/admin/subfamily/add-subfamily/add-subfamily.component';
import { EditSubfamilyComponent } from './components/admin/subfamily/edit-subfamily/edit-subfamily.component';
import { OrderComponent } from './pages/admin/order/order.component';
import { FamilyComponent } from './pages/admin/family/family.component';
import { SubfamilyComponent } from './pages/admin/subfamily/subfamily.component';
import { SuborderComponent } from './pages/admin/suborder/suborder.component';
import { EditTribusComponent } from './components/admin/tribus/edit-tribus/edit-tribus.component';
import { AddTribusComponent } from './components/admin/tribus/add-tribus/add-tribus.component';
import { AddContinentComponent } from './components/admin/continent/add-continent/add-continent.component';
import { EditContinentComponent } from './components/admin/continent/edit-continent/edit-continent.component';
import { ContinentComponent } from './pages/admin/continent/continent.component';
import { LayoutComponent } from './components/public/layout/layout.component';
import { NavigationComponent } from './components/public/navigation/navigation.component';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        AddFishComponent,
        DeleteFishComponent,
        EditFishComponent,
        AllFishesComponent,
        LoginComponent,
        AdminLayoutComponent,
        AdminNavigationComponent,
        InputComponent,
        CardComponent,
        SelectComponent,
        NotificationComponent,
        AddTribusComponent,
        EditTribusComponent,
        TribusComponent,
        AddFamilyComponent,
        EditFamilyComponent,
        AddOrderComponent,
        EditOrderComponent,
        EditSuborderComponent,
        AddSuborderComponent,
        AddSubfamilyComponent,
        EditSubfamilyComponent,
        OrderComponent,
        FamilyComponent,
        SubfamilyComponent,
        SuborderComponent,
        AddContinentComponent,
        EditContinentComponent,
        ContinentComponent,
        LayoutComponent,
        NavigationComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
