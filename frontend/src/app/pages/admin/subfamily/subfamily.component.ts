import { Component, OnInit } from '@angular/core';
import { SubfamilyService } from '../../../services/subfamily/subfamily.service';

@Component({
  selector: 'app-subfamily',
  templateUrl: './subfamily.component.html',
  styleUrls: ['./subfamily.component.scss']
})
export class SubfamilyComponent implements OnInit {

  constructor(
      private subfamilyService: SubfamilyService
  ) { }

  ngOnInit() {
    this.subfamilyService.updateSubfamily()
  }

}
