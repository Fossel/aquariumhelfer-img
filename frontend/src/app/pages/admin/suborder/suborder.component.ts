import { Component, OnInit } from '@angular/core';
import { SuborderService } from 'src/app/services/suborder/suborder.service';

@Component({
  selector: 'app-suborder',
  templateUrl: './suborder.component.html',
  styleUrls: ['./suborder.component.scss']
})
export class SuborderComponent implements OnInit {

  constructor(
      private suborderService: SuborderService
  ) { }

  ngOnInit() {
    this.suborderService.updateSuborder();
  }

}
