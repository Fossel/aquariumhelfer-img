import { Component, OnInit } from '@angular/core';
import { TribusService } from '../../../services/tribus/tribus.service';

@Component({
  selector: 'app-tribus',
  templateUrl: './tribus.component.html',
  styleUrls: ['./tribus.component.scss']
})
export class TribusComponent implements OnInit {

  constructor(
      private tribusService: TribusService

  ) { }

  ngOnInit() {
    this.tribusService.updateTribus();
  }
}
