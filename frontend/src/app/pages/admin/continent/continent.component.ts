import { Component, OnInit } from '@angular/core';
import { ContinentService } from '../../../services/continent/continent.service';

@Component({
  selector: 'app-continent',
  templateUrl: './continent.component.html',
  styleUrls: ['./continent.component.scss']
})
export class ContinentComponent implements OnInit {

  constructor(
      private continentService: ContinentService
  ) { }

  ngOnInit() {
    this.continentService.updateContinent();
  }
}
