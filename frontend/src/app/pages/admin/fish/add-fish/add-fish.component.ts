import { Component, OnInit } from '@angular/core';
import { SelectInterface } from '../../../../components/global/forms/select/select-interface';
import { InputInterface } from '../../../../components/global/forms/input/input-interface';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FamilyService } from '../../../../services/family/family.service';
import { SubfamilyService } from '../../../../services/subfamily/subfamily.service';
import { TribusService } from '../../../../services/tribus/tribus.service';
import { OrderService } from '../../../../services/order/order.service';
import { SuborderService } from '../../../../services/suborder/suborder.service';
import { ContinentService } from '../../../../services/continent/continent.service';
import { FishService } from '../../../../services/fish/fish.service';

@Component({
    selector: 'app-add-fish',
    templateUrl: './add-fish.component.html',
    styleUrls: ['./add-fish.component.scss']
})
export class AddFishComponent implements OnInit {
    public showNotification: boolean = false;
    public showError: boolean = false;
    public addFishForm: FormGroup;
    public nameLt: FormControl = new FormControl('', [Validators.required]);
    public nameDe: FormControl = new FormControl(null);
    public origin: FormControl = new FormControl(null);
    public waters: FormControl = new FormControl(null);
    public family: FormControl = new FormControl(null,);
    public subfamily: FormControl = new FormControl(null);
    public order: FormControl = new FormControl(null);
    public suborder: FormControl = new FormControl(null);
    public tribus: FormControl = new FormControl(null);
    public tempMin: FormControl = new FormControl(null);
    public tempMax: FormControl = new FormControl(null);
    public phMin: FormControl = new FormControl(null);
    public phMax: FormControl = new FormControl(null);
    public ghMin: FormControl = new FormControl(null);
    public ghMax: FormControl = new FormControl(null);
    public age: FormControl = new FormControl(null);
    public size: FormControl = new FormControl(null);
    public continent: FormControl = new FormControl(null);
    public multiplication: FormControl = new FormControl(null);
    public characteristics: FormControl = new FormControl(null);
    public attitude: FormControl = new FormControl(null);
    public food: FormControl = new FormControl(null);
    public aquariumRegion: FormControl = new FormControl(null);
    public liter: FormControl = new FormControl(null);
    public edgeLength: FormControl = new FormControl(null);
    public description: FormControl = new FormControl(null);
    public synonyms: FormControl = new FormControl(null);
    public discovererYear: FormControl = new FormControl(null);
    public discoverer: FormControl = new FormControl(null);

    public selectFamiliesOptions: SelectInterface;
    public selectSubfamiliesOptions: SelectInterface;
    public selectOrderOptions: SelectInterface;
    public selectSuborderOptions: SelectInterface;
    public selectTribusOptions: SelectInterface;
    public selectContinentOptions: SelectInterface;

    public nameLtConfig: InputInterface = {
        label: 'Name lateinisch',
        type: 'text',
        validation: true,
        validationText: 'Bitte geben Sie einen lateinischen Namen ein'
    };
    public nameDeConfig: InputInterface = {
        label: 'Name deutsch',
        type: 'text',
        validation: false,
    };
    public originConfig: InputInterface = {
        label: 'Herkunft',
        type: 'text',
        validation: true,
        validationText: 'Bitte geben Sie ein Herkunft ein'
    };

    public phMinConfig: InputInterface = {
        label: 'pH Min.',
        type: 'number',
        validation: false
    };

    public phMaxConfig: InputInterface = {
        label: 'pH Max.',
        type: 'number',
        validation: false
    };

    public tempMinConfig: InputInterface = {
        label: 'Temp Min.',
        type: 'number',
        validation: false
    };

    public tempMaxConfig: InputInterface = {
        label: 'Temp Max.',
        type: 'number',
        validation: false
    };

    public ghMinConfig: InputInterface = {
        label: 'gH Min.',
        type: 'number',
        validation: false
    };

    public ghMaxConfig: InputInterface = {
        label: 'gH Max.',
        type: 'number',
        validation: false
    };

    public ageConfig: InputInterface = {
        label: 'Alter',
        type: 'number',
        validation: false
    };

    public sizeConfig: InputInterface = {
        label: 'Größe',
        type: 'number',
        validation: false
    };
    public watersConfig: InputInterface = {
        label: 'Gewässer',
        type: 'text',
        validation: false
    };
    public multiplicationOptions: InputInterface = {
        label: 'Vermehrung',
        type: 'text',
        validation: false,
    };
    public characteristicsOptions: InputInterface = {
        label: 'Kennzeichnen',
        type: 'text',
        validation: false
    };
    public attitudeOptions: InputInterface = {
        label: 'Haltung',
        type: 'text',
        validation: false
    };

    public foodOptions: InputInterface = {
        label: 'Futter',
        type: 'text',
        validation: false
    };
    public aquariumRegionOptions: InputInterface = {
        label: 'Beckenregion',
        type: 'text',
        validation: false
    };
    public literOptions: InputInterface = {
        label: 'Aquariumliter',
        type: 'number',
        validation: false
    };
    public edgeLengthOptions: InputInterface = {
        label: 'Aquariumlänge',
        type: 'number',
        validation: false
    };
    public descriptionOptions: InputInterface = {
        label: 'Beschreibung',
        type: 'text',
        validation: false
    };
    public synonymsOptions: InputInterface = {
        label: 'Synonyme',
        type: 'text',
        validation: false
    };
    public discovererOptions: InputInterface = {
        label: 'Entdecker',
        type: 'text',
        validation: false
    };
    public discovererYearOptions: InputInterface = {
        label: 'Entdeckungsjahr',
        type: 'number',
        validation: false
    };


    constructor(
        private formBuilder: FormBuilder,
        private familyService: FamilyService,
        private subfamilyService: SubfamilyService,
        private tribusService: TribusService,
        private orderService: OrderService,
        private suborderService: SuborderService,
        private continentService: ContinentService,
        private fishService: FishService
    ) {
    }

    ngOnInit() {
        this.selectFamiliesOptions = this.familyService.getSelectOptions('Familie auswählen');
        this.selectSubfamiliesOptions = this.subfamilyService.getSelectOptions('Unterfamilie auswählen');
        this.selectOrderOptions = this.orderService.getSelectOptions('Ordnung auswählen');
        this.selectSuborderOptions = this.suborderService.getSelectOptions('Unterordnung auswählen');
        this.selectTribusOptions = this.tribusService.getSelectOptions('Tribus auswählen');
        this.selectContinentOptions = this.continentService.getSelectOptions('Kontienet auswählen');

        this.addFishForm = this.formBuilder.group({
            nameLt: this.nameLt,
            nameDe: this.nameDe,
            origin: this.origin,
            continent: this.continent,
            waters: this.waters,
            family: this.family,
            subfamily: this.subfamily,
            order: this.order,
            suborder: this.suborder,
            tribus: this.tribus,
            phMin: this.phMin,
            phMax: this.phMax,
            tempMax: this.tempMax,
            tempMin: this.tempMin,
            ghMin: this.ghMin,
            ghMax: this.ghMax,
            age: this.age,
            size: this.size,
            multiplication: this.multiplication,
            characteristics: this.characteristics,
            attitude: this.attitude,
            food: this.food,
            aquariumRegion: this.aquariumRegion,
            liter: this.liter,
            edgeLength: this.edgeLength,
            description: this.description,
            synonyms: this.synonyms,
            discoverer: this.discoverer,
            discovererYear: this.discovererYear,
        });
    }

    submit():void {


        this.fishService.add(this.addFishForm.value).subscribe( () => {
            this.showNotification = true;
            this.addFishForm.reset();
            setTimeout(() => {
                this.showNotification = false;
            }, 5000);
        },(err) => {
            console.log(err);
            this.showError = true;
            setTimeout(() => {
                this.showError = false;
                this.showNotification = false;
            }, 5000);
        });
    }
}
