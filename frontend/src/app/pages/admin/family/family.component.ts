import { Component, OnInit } from '@angular/core';
import { FamilyService } from '../../../services/family/family.service';

@Component({
  selector: 'app-family',
  templateUrl: './family.component.html',
  styleUrls: ['./family.component.scss']
})
export class FamilyComponent implements OnInit {

  constructor(
      private familyService: FamilyService
  ) { }

  ngOnInit() {
    this.familyService.updateFamily();
  }
}
