import { Component, OnInit } from '@angular/core';
import { FishService } from '../../../../services/fish/fish.service';
import set = Reflect.set;

@Component({
    selector: 'app-all-fishes',
    templateUrl: './all-fishes.component.html',
    styleUrls: ['./all-fishes.component.scss']
})
export class AllFishesComponent implements OnInit {
    public fishes;
    public activeChar = 0;
    public fishesSub;
    public alphabet = new Array(27);

    constructor(
        private fishService: FishService
    ) {
    }

    ngOnInit() {
        this.fishService.getAll();
        this.fishService.getFishUpdateListener().subscribe((getData) => {
            this.fishes = getData;
        });


    }

    getChar(num: number): string {
        return String.fromCharCode(num);
    }

    getFishByChar(num: number) {
        this.activeChar = num;
        if (num === 0) {
            this.fishService.getAll();
        } else {
            this.fishService.getFishByChar(this.getChar(num));
        }
    }
}
