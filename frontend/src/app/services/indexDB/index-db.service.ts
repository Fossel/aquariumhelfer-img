import { Injectable } from '@angular/core';
import Dexie from 'dexie';

@Injectable({
    providedIn: 'root'
})
export class IndexDBService {
    private db: any;

    constructor() {
    }

    get datebase() {
        return this.db;
    }

    public createDatabase() {
        this.db = new Dexie('aquariumhelfer');
        this.createSchemas();
    }

    public createSchemas() {
        this.db.version(1).stores({
            tribusAdd: '_id,nameDe,nameLt,newNameDe,newNameLt',
            tribusDel: '_id',
            tribusEdit: 'id, nameDe, nameLt',

            familyAdd: '_id,nameDe,nameLt,newNameDe,newNameLt',
            familyDel: '_id',
            familyEdit: 'id, nameDe, nameLt',

            subfamilyAdd: '_id,nameDe,nameLt,newNameDe,newNameLt',
            subfamilyDel: '_id',
            subfamilyEdit: 'id, nameDe, nameLt',

            orderAdd: '_id,nameDe,nameLt,newNameDe,newNameLt',
            orderDel: '_id',
            orderEdit: 'id, nameDe, nameLt',

            suborderAdd: '_id,nameDe,nameLt,newNameDe,newNameLt',
            suborderDel: '_id',
            suborderEdit: 'id, nameDe, nameLt',

            continentAdd: '_id,name,newName',
            continentDel: '_id',
            continentEdit: 'id, name'
        });
    }

    public addTribus(tribus) {
        this.datebase.tribusAdd.put(tribus)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public delTribus(id) {
        this.datebase.tribusDel.put({_id: id}
        )
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    };

    public editTribus(tribus) {
        this.datebase.tribusEdit.put(tribus)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public addFamily(family) {
        this.datebase.familyAdd.put(family)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public delFamily(id) {
        this.datebase.familyDel.put({_id: id}
        )
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    };

    public editFamily(family) {
        this.datebase.familyEdit.put(family)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public addSubfamily(subfamily) {
        this.datebase.subfamilyAdd.put(subfamily)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public delSubfamily(id) {

        this.datebase.subfamilyDel.put({_id: id})
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    };

    public editSubfamily(subfamily) {
        this.datebase.subfamilyEdit.put(subfamily)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public addOrder(tribus) {
        this.datebase.orderAdd.put(tribus)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public delOrder(id) {
        this.datebase.orderDel.put({_id: id}
        )
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    };

    public editOrder(order) {
        this.datebase.orderEdit.put(order)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public addSuborder(suborder) {
        this.datebase.suborderAdd.put(suborder)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public delSuborder(id) {
        this.datebase.suborderDel.put({_id: id}
        )
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    };

    public editSuborder(suborder) {
        this.datebase.suborderEdit.put(suborder)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public addContinent(continent) {
        this.datebase.continentAdd.put(continent)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }

    public delContinent(id) {
        this.datebase.continentDel.put({_id: id}
        )
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    };

    public editContinent(continent) {
        this.datebase.continentEdit.put(continent)
            .then(() => {
                console.info('added');
            })
            .catch((error) => {
                console.error(error);
            });
    }
}
