import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FamilyModel } from '../../models/family/family.model';
import { SelectInterface, SelectOptionInterface } from '../../components/global/forms/select/select-interface';
import { OnlineOfflineServiceService } from '../onlineOfflineService/online-offline-service.service';
import { IndexDBService } from '../indexDB/index-db.service';

@Injectable({
    providedIn: 'root'
})
export class FamilyService {
    private family: FamilyModel[] = [];
    private familyUpdated = new Subject<FamilyModel[]>();
    private apiEndpoint = '/api/v1/family';
    private index: any = 0;

    constructor(
        private http: HttpClient,
        private onlineOfflineService: OnlineOfflineServiceService,
        private indexDbService: IndexDBService
    ) {
    }

    getFamilyUpdateListener() {
        return this.familyUpdated.asObservable();
    }

    getSelectOptions(label?: string, options?: SelectOptionInterface[]): SelectInterface {
        let selectFamiliesOptions = {
            label: label || 'Familie',
            options: options || []
        };
        this.getAll();
        this.getFamilyUpdateListener().subscribe((families) => {
            for (let family of families) {
                if(family.nameDe) {
                    selectFamiliesOptions.options.push({value: family._id, text: family.nameLt + ' - '+ family.nameDe} );
                } else {
                    selectFamiliesOptions.options.push({value: family._id, text: family.nameLt} );
                }
            }
        });
        return selectFamiliesOptions;
    }

    getAll() {
        this.http.get<any>(`${environment.apiUrl}${this.apiEndpoint}`).subscribe(transformedPosts => {
            this.family = transformedPosts.family;
            this.familyUpdated.next([...this.family]);
        });
    }

    add(nameLt: string, nameDe?: string) {
        if (this.onlineOfflineService.isOnline) {
            const params = new HttpParams()
                .set('nameLt', nameLt)
                .set('nameDe', nameDe);

            this.http.post<any>(`${environment.apiUrl}${this.apiEndpoint}`,
                params
            ).subscribe(() => {
                this.getAll();
            });
        } else {
            let family: FamilyModel = {
                _id: this.index,
                nameLt: nameLt,
                nameDe: nameDe,
                newNameDe: null,
                newNameLt: null
            };
            this.familyUpdated.next([...this.family, ...[family]]);
            this.indexDbService.addFamily(family);
            this.index++;
        }
    }

    delete(delId: string) {
        if (this.onlineOfflineService.isOnline) {
            const options = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json'
                }),
                body: {
                    id: delId
                }
            };
            this.http.delete<any>(`${environment.apiUrl}${this.apiEndpoint}`,
                options
            ).subscribe(() => {
            });
        } else {
            this.indexDbService.delFamily(delId);
        }
        const updatedFamily = this.family.filter((data) => data._id !== delId);
        this.family = updatedFamily;
        this.familyUpdated.next([...this.family]);
    }

    update(item) {
        if (this.onlineOfflineService.isOnline) {
            const params = new HttpParams()
                .set('id', item.id)
                .set('nameLt', item.nameLt)
                .set('nameDe', item.nameDe);
            this.http.put<any>(`${environment.apiUrl}${this.apiEndpoint}`,
                params
            ).subscribe(data => {
                console.log(data);
                this.familyUpdated.next([...this.family]);

                this.getAll();
            });
        } else {
            this.indexDbService.editFamily({id: item.id, nameLt: item.nameLt, nameDe: item.nameDe});
            for (let familyItem of this.family) {
                if (familyItem._id === item.id) {
                    familyItem.nameLt = item.nameLt;
                    familyItem.nameDe = item.nameDe;
                    familyItem.edit = false;
                }
            }
            this.familyUpdated.next(this.family);
        }
    }

    public async updateFamily() {
        let allItems: any[] = await this.indexDbService.datebase.familyDel.toArray();

        allItems.forEach((item: any) => {
            this.delete(item._id);
            this.indexDbService.datebase.familyDel.delete(item._id).then(() => {
                console.log(`item ${item._id} sent and deleted locally`);
            });
        });

        allItems = await this.indexDbService.datebase.familyAdd.toArray();
        allItems.forEach((item: any) => {
            console.log(item);
            this.add(item.nameLt, item.nameLt);
            this.indexDbService.datebase.familyAdd.delete(item._id).then(() => {
                console.log(`item ${item._id} sent and deleted locally`);
            });
        });
        allItems = await this.indexDbService.datebase.familyEdit.toArray();
        allItems.forEach((item: any) => {
            console.log(item);
            this.update(item);
        });
    }
}
