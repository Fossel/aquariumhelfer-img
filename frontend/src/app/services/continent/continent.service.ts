import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ContinentModel } from '../../models/continent/continent.model';
import { SelectInterface, SelectOptionInterface } from '../../components/global/forms/select/select-interface';
import { OnlineOfflineServiceService } from '../onlineOfflineService/online-offline-service.service';
import { IndexDBService } from '../indexDB/index-db.service';

@Injectable({
  providedIn: 'root'
})
export class ContinentService {
  private continent: ContinentModel[] = [];
  private continentUpdated = new Subject<ContinentModel[]>();
  private apiEndpoint = '/api/v1/continent';
  private index: any = 0;

  constructor(
      private http: HttpClient,
      private onlineOfflineService: OnlineOfflineServiceService,
      private indexDbService: IndexDBService
  ) {
  }

  getContinentUpdateListener() {
    return this.continentUpdated.asObservable();
  }

  getSelectOptions(label?: string, options?: SelectOptionInterface[]): SelectInterface {
    let selectContinentOptions = {
      label: label || 'Kontinent',
      options: options || []
    };
    this.getAll();
    this.getContinentUpdateListener().subscribe((continents) => {
      for (let continent of continents) {
        selectContinentOptions.options.push({value: continent._id, text: continent.name} );
      }
    });
    return selectContinentOptions;
  }

  getAll() {
    this.http.get<any>(`${environment.apiUrl}${this.apiEndpoint}`).subscribe(transformedPosts => {
      this.continent = transformedPosts.continent;
      this.continentUpdated.next([...this.continent]);
    });
  }

  add(name: string) {
    if (this.onlineOfflineService.isOnline) {

      const params = new HttpParams()
          .set('name', name);
      this.http.post<any>(`${environment.apiUrl}${this.apiEndpoint}`,
          params
      ).subscribe(() => {
        this.getAll();
      });
    } else {
      let contient: ContinentModel = {
        _id: this.index,
        name: name,
        newName: null
      };
      this.continentUpdated.next([...this.continent, ...[contient]]);
      this.indexDbService.addContinent(contient);
      this.index++;
    }
  }

  update(item) {
    if (this.onlineOfflineService.isOnline) {
      console.log(item);
      const params = new HttpParams()
          .set('id', item.id)
          .set('name', item.name);

      this.http.put<any>(`${environment.apiUrl}${this.apiEndpoint}`,
          params
      ).subscribe(data => {
        console.log(data);
        this.continentUpdated.next([...this.continent]);

        this.getAll();
      });
    } else {
      this.indexDbService.editContinent({id: item.id, name: item.name});
      for (let continentItem of this.continent) {
        if (continentItem._id === item.id) {
          console.log("teffer");
          continentItem.name = item.name;
          continentItem.edit = false;
        }
      }
      this.continentUpdated.next(this.continent);
    }
  }

  del(delId: string) {
    if (this.onlineOfflineService.isOnline) {

      const options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        body: {
          id: delId
        }
      };
      this.http.delete<any>(`${environment.apiUrl}${this.apiEndpoint}`,
          options
      ).subscribe(() => {
        this.continent = this.continent.filter((data) => data._id !== delId);
        this.continentUpdated.next([...this.continent]);
      });
    } else {
      this.indexDbService.delContinent(delId);
    }
    const updateContinent = this.continent.filter((data) => data._id !== delId);
    this.continent = updateContinent;
    this.continentUpdated.next([...this.continent]);
  }


  public async updateContinent() {
    let allItems: any[] = await this.indexDbService.datebase.continentDel.toArray();

    allItems.forEach((item: any) => {
      this.del(item._id);
      this.indexDbService.datebase.continentDel.delete(item._id).then(() => {
        console.log(`item ${item._id} sent and deleted locally`);
      });
    });

    allItems = await this.indexDbService.datebase.continentAdd.toArray();
    allItems.forEach((item: any) => {
      console.log(item);
      this.add(item.name);
      this.indexDbService.datebase.continentAdd.delete(item._id).then(() => {
        console.log(`item ${item._id} sent and deleted locally`);
      });
    });
    allItems = await this.indexDbService.datebase.continentEdit.toArray();
    allItems.forEach((item: any) => {
      this.update(item);
    });
  }
}
