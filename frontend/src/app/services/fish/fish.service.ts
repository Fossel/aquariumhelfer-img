import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FishModel } from '../../models/fish/fish.model';

@Injectable({
  providedIn: 'root'
})
export class FishService {
  private fish: FishModel[] = [];
  private fishUpdated = new Subject<FishModel[]>();
  private apiEndpoint = '/api/v1/fish';

  constructor(
      private http: HttpClient
  ) {
  }

  getFishUpdateListener() {
    return this.fishUpdated.asObservable();
  }

  update(item) {
    const params = new HttpParams()
        .set('id', item.id)
        .set('nameLt', item.nameLt)
        .set('nameDe', item.nameDe);

    this.http.put<any>(`${environment.apiUrl}${this.apiEndpoint}`,
        params
    ).subscribe(data => {
      console.log(data);
      this.fishUpdated.next([...this.fish]);

      this.getAll();
    });
  }

  getAll() {
    this.http.get<any>(`${environment.apiUrl}${this.apiEndpoint}`).subscribe(transformedFishes => {
      this.fish = transformedFishes.fish;
      this.fishUpdated.next([...this.fish]);
    });
  }

  getFishByChar(char: string) {
    const params = new HttpParams().set('char', char);
    this.http.get<any>(`${environment.apiUrl}${this.apiEndpoint}/by-char`,{params: params}).subscribe(transformedFishes => {
      this.fish = transformedFishes.fish;
      this.fishUpdated.next(this.fish);
    });
  }

  add(values) {
    const params = new FormData();
      for (let [key, value] of Object.entries(values)) {
        if(value) {
          console.log(value);
          params.append(key.toString(),value.toString());
        }
      }
    return this.http.post<any>(`${environment.apiUrl}${this.apiEndpoint}`,
        params
    );
  }

  del(delId: string) {
    console.log(delId);
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: {
        id: delId
      }
    };
    this.http.delete<any>(`${environment.apiUrl}${this.apiEndpoint}`,
        options
    ).subscribe(() => {
      this.fishUpdated.next([...this.fish]);
    });
  }
}
