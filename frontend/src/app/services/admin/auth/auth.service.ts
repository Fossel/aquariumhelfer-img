import {Injectable} from '@angular/core';
import {StorageService} from '../../storage/storage.service';
import {CanActivate, Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService implements CanActivate {

    constructor(
        private storageService: StorageService,
        private router: Router
    ) {
    }

    canActivate() {
        return this.isLoggedIn();
    }

    public isLoggedIn() {
        if (+this.storageService.get('expieres') > new Date().getTime()) {
            return true;
        } else {
            this.logout();
            return false;
        }
    }

    login(email: string, password: string) {
        if ((email === 'mircosteinau@web.de' && password === '1234') || (email === 'e@b.com' && password === '1234')) {
            this.storageService.set('email', email);
            this.storageService.set('expieres', (+new Date().getTime() + (1 * 3600 * 1000)));
            this.router.navigate(['admin/dashboard']);

            return true;
        } else {
            this.logout();
            return false;
        }
    }

    logout() {
        this.router.navigate(['/login']);
        this.storageService.delete('email');
        this.storageService.delete('expieres');
    }
}
