import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Subject } from 'rxjs';
import { TribusModel } from 'src/app/models/tribus/tribus.model';
import { SelectInterface, SelectOptionInterface } from '../../components/global/forms/select/select-interface';
import { OnlineOfflineServiceService } from '../onlineOfflineService/online-offline-service.service';
import { IndexDBService } from '../indexDB/index-db.service';

@Injectable({
    providedIn: 'root'
})
export class TribusService {
    private tribus: TribusModel[] = [];
    private tribusUpdated = new Subject<TribusModel[]>();
    private index: any = 0;

    constructor(
        private http: HttpClient,
        private onlineOfflineService: OnlineOfflineServiceService,
        private indexDbService: IndexDBService
    ) { }

    getTribusUpdateListener() {
        return this.tribusUpdated.asObservable();
    }

    getSelectOptions(label?: string, options?: SelectOptionInterface[]): SelectInterface {
        let selectTribusOptions = {
            label: label || 'Tribus',
            options: options || []
        };
        this.getAll();
        this.getTribusUpdateListener().subscribe((tribus) => {
            for (let tribusItem of tribus) {
                if (tribusItem.nameDe) {
                    selectTribusOptions.options.push({
                        value: tribusItem._id,
                        text: tribusItem.nameLt + ' - ' + tribusItem.nameDe
                    });
                } else {
                    selectTribusOptions.options.push({value: tribusItem._id, text: tribusItem.nameLt});
                }
            }
        });

        return selectTribusOptions;
    }

    update(item) {
        if(this.onlineOfflineService.isOnline) {
            const params = new HttpParams()
                .set('id', item.id)
                .set('nameLt', item.nameLt)
                .set('nameDe', item.nameDe);
                console.log(params);
            this.http.put<any>(`${environment.apiUrl}/api/v1/tribus`,
                params
            ).subscribe(data => {
                console.log(data);
                this.tribusUpdated.next([...this.tribus]);

                this.getAll();
            });
        } else {
            this.indexDbService.editTribus({id: item.id, nameLt: item.nameLt, nameDe: item.nameDe});

            for(let tribusItem of this.tribus) {
                if(tribusItem._id === item.id){
                    tribusItem.nameLt = item.nameLt;
                    tribusItem.nameDe = item.nameDe;
                }
            }
        }
    }

    getAll() {
        this.http.get<any>(`${environment.apiUrl}/api/v1/tribus`).subscribe(transformedPosts => {
            this.tribus = transformedPosts.tribus;
            this.tribusUpdated.next([...this.tribus]);
            this.index = this.tribus[this.tribus.length-1]._id;

        });
    }

    public async updateTribus() {
            let allItems:any[] = await this.indexDbService.datebase.tribusDel.toArray();
            allItems.forEach((item: any) => {
                this.del(item._id);
                this.indexDbService.datebase.tribusDel.delete(item._id).then(() => {
                    console.log(`item ${item._id} sent and deleted locally`);
                });
            });

            allItems = await this.indexDbService.datebase.tribusAdd.toArray();
            allItems.forEach((item: any) => {
                console.log(item);
                this.add(item.nameLt, item.nameLt);
                this.indexDbService.datebase.tribusAdd.delete(item._id).then(() => {
                    console.log(`item ${item._id} sent and deleted locally`);
                });
            });
            allItems = await this.indexDbService.datebase.tribusEdit.toArray();
            allItems.forEach((item: any) => {
                console.log(item);
                this.update(item);
            });
    }

    add(nameLt: string, nameDe?: string) {
        if (this.onlineOfflineService.isOnline) {
            const params = new HttpParams()
                .set('nameLt', nameLt)
                .set('nameDe', nameDe);

            this.http.post<any>(`${environment.apiUrl}/api/v1/tribus`,
                params
            ).subscribe(data => {
                this.getAll();
            });
        } else {
            let tribus:TribusModel =  {_id: this.index, nameLt: nameLt, nameDe: nameDe, newNameDe: null, newNameLt: null};
            this.tribusUpdated.next([...this.tribus, ...[
                   tribus
                ]]
            );
            this.indexDbService.addTribus(tribus);
            this.index ++;

            console.log('offline');
        }
    }

    del(delId: string) {
        if(this.onlineOfflineService.isOnline) {
            const options = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json'
                }),
                body: {
                    id: delId
                }
            };
            this.http.delete<any>(`${environment.apiUrl}/api/v1/tribus`,
                options
            ).subscribe(() => { });
        } else {
            this.indexDbService.delTribus(delId);
        }
        const updatedTribus = this.tribus.filter((data) => data._id !== delId);
        this.tribus = updatedTribus;
        this.tribusUpdated.next([...this.tribus]);
    }
}
