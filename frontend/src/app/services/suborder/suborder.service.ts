import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { SuborderModel } from '../../models/suborder/suborder.model';
import { SelectInterface, SelectOptionInterface } from '../../components/global/forms/select/select-interface';
import { OnlineOfflineServiceService } from '../onlineOfflineService/online-offline-service.service';
import { IndexDBService } from '../indexDB/index-db.service';

@Injectable({
  providedIn: 'root'
})
export class SuborderService {

  private suborder: SuborderModel[] = [];
  private suborderUpdate = new Subject<SuborderModel[]>();
  private apiEndpoint = '/api/v1/suborder';
  private index: any = 0;

  constructor(
      private http: HttpClient,
      private onlineOfflineService: OnlineOfflineServiceService,
      private indexDbService: IndexDBService
  ) {
  }

  getSuborderUpdateListener() {
    return this.suborderUpdate.asObservable();
  }

  getSelectOptions(label?: string, options?: SelectOptionInterface[]): SelectInterface {
    let selectFamiliesOptions = {
      label: label || 'Unterordung',
      options: options || []
    };
    this.getAll();
    this.getSuborderUpdateListener().subscribe((families) => {
      for (let suborder of families) {
        if(suborder.nameDe) {
          selectFamiliesOptions.options.push({value: suborder._id, text: suborder.nameLt + ' - '+ suborder.nameDe} );
        } else {
          selectFamiliesOptions.options.push({value: suborder._id, text: suborder.nameLt} );
        }
      }
    });
    return selectFamiliesOptions;
  }

  getAll() {
    this.http.get<any>(`${environment.apiUrl}${this.apiEndpoint}`).subscribe(transformedPosts => {
      this.suborder = transformedPosts.suborder;
      this.suborderUpdate.next([...this.suborder]);
    });
  }

  add(nameLt: string, nameDe?: string) {
    if (this.onlineOfflineService.isOnline) {
      const params = new HttpParams()
          .set('nameLt', nameLt)
          .set('nameDe', nameDe);

      this.http.post<any>(`${environment.apiUrl}${this.apiEndpoint}`,
          params
      ).subscribe(() => {
        this.getAll();
      });
    } else {
      let suborder: SuborderModel = {
        _id: this.index,
        nameLt: nameLt,
        nameDe: nameDe,
        newNameDe: null,
        newNameLt: null
      };
      this.suborderUpdate.next([...this.suborder, ...[suborder]]);
      this.indexDbService.addSuborder(suborder);
      this.index++;
    }
  }

  delete(delId: string) {
    if (this.onlineOfflineService.isOnline) {
      const options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        body: {
          id: delId
        }
      };
      this.http.delete<any>(`${environment.apiUrl}${this.apiEndpoint}`,
          options
      ).subscribe(() => {
      });
    } else {
      this.indexDbService.delSuborder(delId);
    }
    const suborderUpdate = this.suborder.filter((data) => data._id !== delId);
    this.suborder = suborderUpdate;
    this.suborderUpdate.next([...this.suborder]);
  }

  update(item) {
    if (this.onlineOfflineService.isOnline) {
      const params = new HttpParams()
          .set('id', item.id)
          .set('nameLt', item.nameLt)
          .set('nameDe', item.nameDe);
      this.http.put<any>(`${environment.apiUrl}${this.apiEndpoint}`,
          params
      ).subscribe(data => {
        console.log(data);
        this.suborderUpdate.next([...this.suborder]);

        this.getAll();
      });
    } else {
      this.indexDbService.editSuborder({id: item.id, nameLt: item.nameLt, nameDe: item.nameDe});
      for (let suborderItem of this.suborder) {
        if (suborderItem._id === item.id) {
          suborderItem.nameLt = item.nameLt;
          suborderItem.nameDe = item.nameDe;
          suborderItem.edit = false;
        }
      }
      this.suborderUpdate.next(this.suborder);
    }
  }

  public async updateSuborder() {
    let allItems: any[] = await this.indexDbService.datebase.suborderDel.toArray();

    allItems.forEach((item: any) => {
      this.delete(item._id);
      this.indexDbService.datebase.suborderDel.delete(item._id).then(() => {
        console.log(`item ${item._id} sent and deleted locally`);
      });
    });

    allItems = await this.indexDbService.datebase.suborderAdd.toArray();
    allItems.forEach((item: any) => {
      console.log(item);
      this.add(item.nameLt, item.nameLt);
      this.indexDbService.datebase.suborderAdd.delete(item._id).then(() => {
        console.log(`item ${item._id} sent and deleted locally`);
      });
    });
    allItems = await this.indexDbService.datebase.suborderEdit.toArray();
    allItems.forEach((item: any) => {
      console.log(item);
      this.update(item);
    });
  }
}
