import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { OrderModel } from '../../models/order/order.model';
import { SelectInterface, SelectOptionInterface } from '../../components/global/forms/select/select-interface';
import { OnlineOfflineServiceService } from '../onlineOfflineService/online-offline-service.service';
import { IndexDBService } from '../indexDB/index-db.service';

@Injectable({
    providedIn: 'root'
})
export class OrderService {

    private order: OrderModel[] = [];
    private orderUpdated = new Subject<OrderModel[]>();
    private apiEndpoint = '/api/v1/order';
    private index: any = 0;

    constructor(
        private http: HttpClient,
        private onlineOfflineService: OnlineOfflineServiceService,
        private indexDbService: IndexDBService
    ) {
    }

    getOrderUpdateListener() {
        return this.orderUpdated.asObservable();
    }

    getSelectOptions(label?: string, options?: SelectOptionInterface[]): SelectInterface {
        let selectOrderOptions = {
            label: label || 'Ordnung',
            options: options || []
        };
        this.getAll();
        this.getOrderUpdateListener().subscribe((orders) => {
            for (let order of orders) {
                if(order.nameDe) {
                    selectOrderOptions.options.push({value: order._id, text: order.nameLt + ' - '+ order.nameDe} );
                } else {
                    selectOrderOptions.options.push({value: order._id, text: order.nameLt} );
                }
            }
        });

        return selectOrderOptions;
    }

    getAll() {
        this.http.get<any>(`${environment.apiUrl}${this.apiEndpoint}`).subscribe(transformedPosts => {
            this.order = transformedPosts.order;
            this.orderUpdated.next([...this.order]);
        });
    }

    add(nameLt: string, nameDe?: string) {
        if (this.onlineOfflineService.isOnline) {
            const params = new HttpParams()
                .set('nameLt', nameLt)
                .set('nameDe', nameDe);

            this.http.post<any>(`${environment.apiUrl}${this.apiEndpoint}`,
                params
            ).subscribe(() => {
                this.getAll();
            });
        } else {
            let order: OrderModel = {
                _id: this.index,
                nameLt: nameLt,
                nameDe: nameDe,
                newNameDe: null,
                newNameLt: null
            };
            this.orderUpdated.next([...this.order, ...[order]]);
            this.indexDbService.addOrder(order);
            this.index++;
        }
    }

    delete(delId: string) {
        if (this.onlineOfflineService.isOnline) {
            const options = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json'
                }),
                body: {
                    id: delId
                }
            };
            this.http.delete<any>(`${environment.apiUrl}${this.apiEndpoint}`,
                options
            ).subscribe(() => {
            });
        } else {
            this.indexDbService.delOrder(delId);
        }
        const updateOrder = this.order.filter((data) => data._id !== delId);
        this.order = updateOrder;
        this.orderUpdated.next([...this.order]);
    }

    update(item) {
        if (this.onlineOfflineService.isOnline) {
            const params = new HttpParams()
                .set('id', item.id)
                .set('nameLt', item.nameLt)
                .set('nameDe', item.nameDe);
            this.http.put<any>(`${environment.apiUrl}${this.apiEndpoint}`,
                params
            ).subscribe(data => {
                console.log(data);
                this.orderUpdated.next([...this.order]);

                this.getAll();
            });
        } else {
            this.indexDbService.editOrder({id: item.id, nameLt: item.nameLt, nameDe: item.nameDe});
            for (let orderItem of this.order) {
                if (orderItem._id === item.id) {
                    orderItem.nameLt = item.nameLt;
                    orderItem.nameDe = item.nameDe;
                    orderItem.edit = false;
                }
            }
            this.orderUpdated.next(this.order);
        }
    }

    public async updateOrder() {
        console.log("updateSub");
        let allItems: any[] = await this.indexDbService.datebase.orderDel.toArray();

        allItems.forEach((item: any) => {
            this.delete(item._id);
            this.indexDbService.datebase.orderDel.delete(item._id).then(() => {
                console.log(`item ${item._id} sent and deleted locally`);
            });
        });

        allItems = await this.indexDbService.datebase.orderAdd.toArray();
        allItems.forEach((item: any) => {
            console.log(item);
            this.add(item.nameLt, item.nameLt);
            this.indexDbService.datebase.orderAdd.delete(item._id).then(() => {
                console.log(`item ${item._id} sent and deleted locally`);
            });
        });
        allItems = await this.indexDbService.datebase.orderEdit.toArray();
        allItems.forEach((item: any) => {
            console.log(item);
            this.update(item);
        });
    }
}
