import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Subject } from 'rxjs';
import { SubfamilyModel } from 'src/app/models/subfamily/subfamily.model';
import { SelectInterface, SelectOptionInterface } from '../../components/global/forms/select/select-interface';
import { OnlineOfflineServiceService } from '../onlineOfflineService/online-offline-service.service';
import { IndexDBService } from '../indexDB/index-db.service';


@Injectable({
    providedIn: 'root'
})
export class SubfamilyService {
    private subfamily: SubfamilyModel[] = [];
    private subfamilyUpdated = new Subject<SubfamilyModel[]>();
    protected apiEndpoint: string = '/api/v1/subfamily';
    private index: any = 0;

    constructor(
        private http: HttpClient,
        private onlineOfflineService: OnlineOfflineServiceService,
        private indexDbService: IndexDBService

    ) {
    }
    getSubfamilyUpdateListener() {
        return this.subfamilyUpdated.asObservable();
    }

    getSelectOptions(label?: string, options?: SelectOptionInterface[]): SelectInterface {
        let selectFamiliesOptions = {
            label: label || 'Unterfamilie',
            options: options || []
        };
        this.getAll();
        this.getSubfamilyUpdateListener().subscribe((families) => {
            for (let subfamily of families) {
                if(subfamily.nameDe) {
                    selectFamiliesOptions.options.push({value: subfamily._id, text: subfamily.nameLt + ' - '+ subfamily.nameDe} );
                } else {
                    selectFamiliesOptions.options.push({value: subfamily._id, text: subfamily.nameLt} );
                }
            }
        });
        return selectFamiliesOptions;
    }

    getAll() {
        this.http.get<any>(`${environment.apiUrl}${this.apiEndpoint}`).subscribe(transformedPosts => {
            this.subfamily = transformedPosts.subfamily;
            this.subfamilyUpdated.next([...this.subfamily]);
        });
    }

    add(nameLt: string, nameDe?: string) {
        if (this.onlineOfflineService.isOnline) {
            const params = new HttpParams()
                .set('nameLt', nameLt)
                .set('nameDe', nameDe);

            this.http.post<any>(`${environment.apiUrl}${this.apiEndpoint}`,
                params
            ).subscribe(() => {
                this.getAll();
            });
        } else {
            let subfamily: SubfamilyModel = {
                _id: this.index,
                nameLt: nameLt,
                nameDe: nameDe,
                newNameDe: null,
                newNameLt: null
            };
            this.subfamilyUpdated.next([...this.subfamily, ...[subfamily]]);
            this.indexDbService.addSubfamily(subfamily);
            this.index++;
        }
    }

    delete(delId: string) {
        if (this.onlineOfflineService.isOnline) {
            const options = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json'
                }),
                body: {
                    id: delId
                }
            };
            this.http.delete<any>(`${environment.apiUrl}${this.apiEndpoint}`,
                options
            ).subscribe(() => {
            });
        } else {
            this.indexDbService.delSubfamily(delId);
        }
        const updateSubfamily = this.subfamily.filter((data) => data._id !== delId);
        this.subfamily = updateSubfamily;
        this.subfamilyUpdated.next([...this.subfamily]);
    }

    update(item) {
        if (this.onlineOfflineService.isOnline) {
            const params = new HttpParams()
                .set('id', item.id)
                .set('nameLt', item.nameLt)
                .set('nameDe', item.nameDe);
            this.http.put<any>(`${environment.apiUrl}${this.apiEndpoint}`,
                params
            ).subscribe(data => {
                console.log(data);
                this.subfamilyUpdated.next([...this.subfamily]);

                this.getAll();
            });
        } else {
            this.indexDbService.editSubfamily({id: item.id, nameLt: item.nameLt, nameDe: item.nameDe});
            for (let subfamilyItem of this.subfamily) {
                if (subfamilyItem._id === item.id) {
                    subfamilyItem.nameLt = item.nameLt;
                    subfamilyItem.nameDe = item.nameDe;
                    subfamilyItem.edit = false;
                }
            }
            this.subfamilyUpdated.next(this.subfamily);
        }
    }

    public async updateSubfamily() {
        console.log("updateSub");
        let allItems: any[] = await this.indexDbService.datebase.subfamilyDel.toArray();

        allItems.forEach((item: any) => {
            this.delete(item._id);
            this.indexDbService.datebase.subfamilyDel.delete(item._id).then(() => {
                console.log(`item ${item._id} sent and deleted locally`);
            });
        });

        allItems = await this.indexDbService.datebase.subfamilyAdd.toArray();
        allItems.forEach((item: any) => {
            console.log(item);
            this.add(item.nameLt, item.nameLt);
            this.indexDbService.datebase.subfamilyAdd.delete(item._id).then(() => {
                console.log(`item ${item._id} sent and deleted locally`);
            });
        });
        allItems = await this.indexDbService.datebase.subfamilyEdit.toArray();
        allItems.forEach((item: any) => {
            console.log(item);
            this.update(item);
        });
    }
}
