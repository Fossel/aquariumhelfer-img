import { Injectable } from '@angular/core';

@Injectable ({
    providedIn: 'root'
})
export class StorageService {

    constructor () {
    }

    set(key: string, data: any): void {
        try {
            localStorage.setItem(key, JSON.stringify(data));
        } catch (e) {
            console.error('Error saving to localStorage', e);
        }
    }

    delete (key) {
        localStorage.removeItem (key);
    }

    get (key) {
        return localStorage.getItem (key);
    }
}
