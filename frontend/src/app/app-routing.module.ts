import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { AddFishComponent } from './pages/admin/fish/add-fish/add-fish.component';
import { DeleteFishComponent } from './pages/admin/fish/delete-fish/delete-fish.component';
import { LoginComponent } from './components/admin/login/login.component';
import { AdminLayoutComponent } from './components/admin/admin-layout/admin-layout.component';
import { AuthService } from './services/admin/auth/auth.service';
import { TribusComponent } from './pages/admin/tribus/tribus.component';
import { SubfamilyComponent } from './pages/admin/subfamily/subfamily.component';
import { FamilyComponent } from './pages/admin/family/family.component';
import { SuborderComponent } from './pages/admin/suborder/suborder.component';
import { OrderComponent } from './pages/admin/order/order.component';
import { ContinentComponent } from './pages/admin/continent/continent.component';
import { AllFishesComponent } from './pages/public/fishes/all-fishes/all-fishes.component';

const routes: Routes = [
  {
    path: '',
    component: AllFishesComponent
  },{

    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthService],
    children: [
      {
        path: 'admin/dashboard',
        component: DashboardComponent,
      },
      {
        path: 'admin/fish/add',
        component: AddFishComponent
      },
      {
        path: 'admin/fish/delete',
        component: DeleteFishComponent
      },
      {
        path: 'admin/tribus',
        component: TribusComponent
      },
      {
        path: 'admin/order',
        component: OrderComponent
      },
      {
        path: 'admin/suborder',
        component: SuborderComponent
      },
      {
        path: 'admin/family',
        component: FamilyComponent
      },
      {
        path: 'admin/subfamily',
        component: SubfamilyComponent
      },
      {
        path: 'admin/continent',
        component: ContinentComponent
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(
        routes,
        { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
