const app = require("./api/api");
const express = require("express");
const path = require('path');
const https = require("https");
const fs = require("fs");

const onListening = () => {
    const bind = "https://localhost:" + 443;
    console.log("Server listen on:", bind);
};

const port = "80";
app.set("port", port);

const server = https.createServer(app);
server.on("listening", onListening);
// APP
app.use(express.static(__dirname + '/../frontend/dist'));
app.get('*', (req, res) => res.sendFile(path.join(__dirname + '/../frontend/dist/index.html')));

https.createServer({
    key: fs.readFileSync("./ssl/lyra.key"),
    cert: fs.readFileSync("./ssl/lyra.crt"),
}, app).listen(443, () => {
    console.log("[app.js] Server is running at port 443");
});

