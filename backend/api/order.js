const app = module.exports = require("express")();
const mongoose = require("mongoose");
const Order = require('./../models/order');
const importFile = require('./imports/order');

mongoose
    .connect(
        'mongodb://localhost:27017/aquariumhelfer'
    )
    .then(() => {
        console.log("Connected to database! Order");
        for (let item of importFile) {
            new Order(
                item
            ).save().then(() => {
                    return;
                }
            ).catch(() => {
                return;
                console.log("Continent available");
            });
        }

    })
    .catch(() => {
        console.log("Connection failed!");
    });

app.get("/api/v1/order", (req, res, next) => {
    Order.find({}).sort({
        "nameLt": 1
    }).then(data => {
        res.status(200).json({
            order: data
        })
    });
});

app.post("/api/v1/order", (req, res, next) => {
    const order = new Order(
        req.body
    );

    order.save().then(createOrder => {
        res.status(201).json({
            message: "order added successfully",
            id: createOrder._id,
            nameLt: createOrder.nameLt,
            nameDe: createOrder.nameDe
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

app.put("/api/v1/order", (req, res, next) => {

    const order = new Order({
        _id: req.body.id,
        nameLt: req.body.nameLt,
        nameDe: req.body.nameDe,
    });
    Order.updateOne({_id: req.body.id}, order).then(result => {
        res.status(200).json({message: "order update successful!"});
    });
});

app.delete("/api/v1/order", (req, res, next) => {
    Order.deleteOne({
        _id: req.body.id
    }).then(result => {
        console.log(result);
        res.status(200).json({message: `order ${req.body.id} deleted!`});
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
