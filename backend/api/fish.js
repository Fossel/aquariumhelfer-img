const app = module.exports = require("express")();
const mongoose = require("mongoose");
const multer = require("multer");
const upload = multer();
const importFile = require('./imports/fish');

const Fish = require('./../models/fish');

mongoose
    .connect(
        'mongodb://localhost:27017/aquariumhelfer'
    )
    .then(() => {
        console.log("Connected Fish");
        for (let item of importFile) {
            new Fish(
                item
            ).save().then(() => {
                    return;
                }
            ).catch(() => {
                return;
                console.log("Fish available");
            });
        }
    })
    .catch(() => {
        console.log("Connection failed!");
    });

app.get("/api/v1/fish", (req, res, next) => {
    Fish.find({},
        {
            "nameDe": 1,
            "nameLt": 1,
            "id": 1,
            "previewImage": 1,
        }).sort({
        "nameDe": 1
    }).then(data => {
        res.status(200).json({
            fish: data
        })
    });
});

app.get("/api/v1/fish/by-char", (req, res, next) => {
    let char = req.query.char[0];
    Fish.find({
            nameDe: new RegExp('^' + char, 'i')
        },
        {
            "nameDe": 1,
            "nameLt": 1,
            "id": 1,
            "previewImage": 1,
        }).then(data => {
        res.status(200).json({
            fish: data
        })
    });
});

app.post("/api/v1/fish", upload.none(), (req, res, next) => {
    const fish = new Fish(
        req.body
    );

    fish.save().then(createTribus => {
        res.status(201).json({
            message: "Tribus added successfully",
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
