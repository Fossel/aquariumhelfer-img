const app = module.exports = require("express")();
const mongoose = require("mongoose");
const Family = require('./../models/family');
const importFile = require('./imports/family');

mongoose
    .connect(
        'mongodb://localhost:27017/aquariumhelfer'
    )
    .then(() => {
        console.log("Connected! Family");
        for (let item of importFile) {
            new Family(
                item
            ).save().then(() => {
                    return;
                }
            ).catch(() => {
                return;
                console.log("Continent available");
            });
        }
    })
    .catch(() => {
        console.log("Connection failed! Family");
    });

app.get("/api/v1/family", (req, res, next) => {
    Family.find({}).sort({
        "nameLt": 1
    }).then(data => {
        res.status(200).json({
            family: data
        })
    });
});

app.post("/api/v1/family", (req, res, next) => {
    const family = new Family(
        req.body
    );

    family.save().then(createFamily => {
        res.status(201).json({
            message: "family added successfully",
            id: createFamily._id,
            nameLt: createFamily.nameLt,
            nameDe: createFamily.nameDe
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

app.put("/api/v1/family", (req, res, next) => {

    const family = new Family({
        _id: req.body.id,
        nameLt: req.body.nameLt,
        nameDe: req.body.nameDe,
    });
    Family.updateOne({_id: req.body.id}, family).then(result => {
        res.status(200).json({message: "family update successful!"});
    });
});

app.delete("/api/v1/family", (req, res, next) => {
    Family.deleteOne({
        _id: req.body.id
    }).then(result => {
        console.log(result);
        res.status(200).json({message: `family ${req.body.id} deleted!`});
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
