const app = module.exports = require("express")();
const mongoose = require("mongoose");
const Suborder = require('./../models/suborder');
const importFile = require('./imports/suborder');

mongoose
    .connect(
        'mongodb://localhost:27017/aquariumhelfer'
    )
    .then(() => {
        console.log("Connected Suborders!");
        for (let item of importFile) {
            new Suborder(
                item
            ).save().then(() => {
                    return;
                }
            ).catch(() => {
                return;
                console.log("Continent Suborders available");
            });
        }
    })
    .catch(() => {
        console.log("Connection failed!");
    });

app.get("/api/v1/suborder", (req, res, next) => {
    Suborder.find({}).sort({
        "nameLt": 1
    }).then(data => {
        res.status(200).json({
            suborder: data
        })
    });
});

app.post("/api/v1/suborder", (req, res, next) => {
    const Suborder = new Suborder(
        req.body
    );

    Suborder.save().then(createSuborder => {
        res.status(201).json({
            message: " under order added successfully",
            id: createSuborder._id,
            nameLt: createSuborder.nameLt,
            nameDe: createSuborder.nameDe
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

app.put("/api/v1/suborder", (req, res, next) => {

    const Suborder = new Suborder({
        _id: req.body.id,
        nameLt: req.body.nameLt,
        nameDe: req.body.nameDe,
    });
    Suborder.updateOne({_id: req.body.id}, Suborder).then(result => {
        res.status(200).json({message: "under order update successful!"});
    });
});

app.delete("/api/v1/suborder", (req, res, next) => {
    Suborder.deleteOne({
        _id: req.body.id
    }).then(result => {
        console.log(result);
        res.status(200).json({message: `under order ${req.body.id} deleted!`});
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
