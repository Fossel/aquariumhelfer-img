const app = module.exports = require("express")();
const mongoose = require("mongoose");
const Tribus = require('./../models/tribus');
const importFile = require('./imports/tribus');

mongoose
    .connect(
        'mongodb://localhost:27017/aquariumhelfer'
    )
    .then(() => {
        console.log("Connected Tribus");
        for (let item of importFile) {
            new Tribus(
                item
            ).save().then(() => {
                    return;
                }
            ).catch(() => {
                return;
                console.log("Continent available");
            });
        }
    })
    .catch(() => {
        console.log("Connection failed!");
    });

app.get("/api/v1/tribus", (req, res, next) => {
    Tribus.find({}).sort({
        "nameLt": 1
    }).then(data => {
        res.status(200).json({
            tribus: data
        })
    });
});

app.post("/api/v1/tribus", (req, res, next) => {
    const tribus = new Tribus(
        req.body
    );

    tribus.save().then(createTribus => {
        res.status(201).json({
            message: "Tribus added successfully",
            id: createTribus._id,
            nameLt: createTribus.nameLt,
            nameDe: createTribus.nameDe
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

app.put("/api/v1/tribus", (req, res, next) => {

    const tribus = new Tribus({
        _id: req.body.id,
        nameLt: req.body.nameLt,
        nameDe: req.body.nameDe,
    });
    Tribus.updateOne({_id: req.body.id}, tribus).then(result => {
        res.status(200).json({message: "Update successful!"});
    });
});

app.delete("/api/v1/tribus", (req, res, next) => {
    Tribus.deleteOne({
        _id: req.body.id
    }).then(result => {
        console.log(result);
        res.status(200).json({message: `Tribus ${req.body.id} deleted!`});
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
