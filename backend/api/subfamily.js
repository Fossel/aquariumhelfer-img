const app = module.exports = require("express")();
const mongoose = require("mongoose");
const Subfamily = require('./../models/subfamily');
const importFile = require('./imports/subfamily');

mongoose
    .connect(
        'mongodb://localhost:27017/aquariumhelfer'
    )
    .then(() => {
        console.log("Connected Subfamily");
        for (let item of importFile) {
            new Subfamily(
                item
            ).save().then(() => {
                    return;
                }
            ).catch(() => {
                return;
                console.log("Continent Subfamily available");
            });
        }
    })
    .catch(() => {
        console.log("Connection failed!");
    });

app.get("/api/v1/subfamily", (req, res, next) => {
    Subfamily.find({}).sort({
        "nameLt": 1
    }).then(data => {
        res.status(200).json({
            subfamily: data
        })
    });
});

app.post("/api/v1/subfamily", (req, res, next) => {
    const subfamily = new Subfamily(
        req.body
    );

    subfamily.save().then(createSubfamily => {
        res.status(201).json({
            message: "subfamily added successfully",
            id: createSubfamily._id,
            nameLt: createSubfamily.nameLt,
            nameDe: createSubfamily.nameDe
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

app.put("/api/v1/subfamily", (req, res, next) => {

    const subfamily = new Subfamily({
        _id: req.body.id,
        nameLt: req.body.nameLt,
        nameDe: req.body.nameDe,
    });
    Subfamily.updateOne({_id: req.body.id}, subfamily).then(result => {
        res.status(200).json({message: "subfamily update successful!"});
    });
});

app.delete("/api/v1/subfamily", (req, res, next) => {
    Subfamily.deleteOne({
        _id: req.body.id
    }).then(result => {
        console.log(result);
        res.status(200).json({message: `subfamily ${req.body.id} deleted!`});
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
