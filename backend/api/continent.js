const app = module.exports = require("express")();
const mongoose = require("mongoose");
const Continent = require('./../models/continent');
const importFile = require('./imports/continent');

mongoose
    .connect(
        'mongodb://localhost:27017/aquariumhelfer'
    )
    .then(() => {
        console.log("Connected! Continent");

        for (let item of importFile) {
            new Continent(
                item
            ).save().then(() => {
                    return;
                }
            ).catch(() => {
                return;
                console.log("Continent available");
            });
        }
    })
    .catch(() => {
        console.log("Connection failed!");
    });

app.get("/api/v1/continent", (req, res, next) => {
    Continent.find({}).sort({
        "name": 1
    }).then(data => {
        res.status(200).json({
            continent: data
        })
    });
});

app.post("/api/v1/continent", (req, res, next) => {
    const continent = new Continent(
        req.body
    );

    continent.save().then(createContinent => {
        res.status(201).json({
            message: "continent added successfully",
            id: createContinent._id,
            name: createContinent.name,
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

app.put("/api/v1/continent", (req, res, next) => {

    const continent = new Continent({
        _id: req.body.id,
        name: req.body.name,
    });
    Continent.updateOne({_id: req.body.id}, continent).then(result => {
        res.status(200).json({message: "continent update successful!"});
    });
});


app.delete("/api/v1/continent", (req, res, next) => {
    Continent.deleteOne({
        _id: req.body.id
    }).then(result => {
        console.log(result);
        res.status(200).json({message: `continent ${req.body.id} deleted!`});
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
