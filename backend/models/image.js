const mongoose = require("mongoose");

const imageSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    alt: {
        type: String,
        default: null
    },
    title: {
        type: String,
        default: null
    }
});

module.exports = mongoose.model('Image', imageSchema);
