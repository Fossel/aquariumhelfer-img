const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

const continentSchema = new mongoose.Schema({
    name: {
        unique: true,
        type: String,
        required: true
    },
    _id: {
        type: Number
    }
});

continentSchema.plugin(autoIncrement, {id:'continent_seq',inc_field: '_id'});
module.exports = mongoose.model('Continent', continentSchema);


