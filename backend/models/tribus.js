const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

const tribusSchema = new mongoose.Schema({
    _id: {
        type: Number
    },
    nameDe: {
        type: String,
        default: null
    },
    nameLt: {
        unique: true,
        type: String,
        required: true
    }
});

tribusSchema.plugin(autoIncrement, {id:'tribus_seq',inc_field: '_id'});
module.exports = mongoose.model('Tribus', tribusSchema);
