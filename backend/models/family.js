const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

const familySchema = new mongoose.Schema({
    nameDe: {
        type: String,
        default: null
    },
    nameLt: {
        unique: true,
        type: String,
        required: true
    },
    _id: {
        type: Number
    }
});
familySchema.plugin(autoIncrement, {id:'family_seq',inc_field: '_id'});
module.exports = mongoose.model('Family', familySchema);
