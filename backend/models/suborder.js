const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

const SuborderSchema = new mongoose.Schema({
    _id: {
        type: Number
    },
    nameDe: {
        type: String,
        default: null
    },
    nameLt: {
        unique: true,
        type: String,
        required: true
    }
});

SuborderSchema.plugin(autoIncrement, {id:'suborder_seq',inc_field: '_id'});
module.exports = mongoose.model('Suborder', SuborderSchema);
