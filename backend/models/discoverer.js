const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

const discovererSchmea = new mongoose.Schema({
    firstName: {
        type: String,
        default: null
    },
    lastName: {
        type: String,
        required: true
    },
    _id: {
        type: Number
    }
});
discovererSchmea.plugin(autoIncrement, {id:'discoverer_seq',inc_field: '_id'});
module.exports = mongoose.model('Discoverer', discovererSchmea);
