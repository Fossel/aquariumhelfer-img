const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

const SubFamilySchema = new mongoose.Schema({
    _id: {
        type: Number
    },
    nameDe: {
        type: String,
        default: null
    },
    nameLt: {
        unique: true,
        type: String,
        required: true
    }
});
SubFamilySchema.plugin(autoIncrement, {id:'subfamily_seq',inc_field: '_id'});
module.exports = mongoose.model('SubFamily', SubFamilySchema);
