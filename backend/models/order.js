const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

const orderSchema = new mongoose.Schema({
    _id: {
        type: Number
    },
    nameDe: {
        type: String,
        default: null
    },
    nameLt: {
        unique: true,
        type: String,
        required: true
    }
});
orderSchema.plugin(autoIncrement, {id:'order_seq',inc_field: '_id'});
module.exports = mongoose.model('Order', orderSchema);
