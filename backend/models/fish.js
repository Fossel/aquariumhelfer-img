const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

const fishSchema = new mongoose.Schema({
    _id: {
        type: Number
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: null
    },
    released: {
        type: Boolean,
        required: false
    },
    nameDe: {
        type: String,
        default: null
    },
    nameLt: {
        type: String,
        required: true,
        unique: true
    },
    origin: {
        type: String,
        default: null
    },
    family: {
        type: String,
        default: null}
        ,
   subfamily: {
        type: String,
        default: null
    },
    order: {
        type: String,
        default: null
    },
    suborder: {
        type: String,
        default: null
    },
    tribus: {
        type: String,
        default: null
    },
    phMin: {
        type: String,
        default: null
    },
    phMax: {
        type: String,
        default: null
    },
    tempMin: {
        type: String,
        default: null
    },
    tempMax: {
        type: String,
        default: null
    },
    ghMin: {
        type: String,
        default: null
    },
    ghMax: {
        type: String,
        default: null
    },
    size: {
        type: String,
        default: null
    },
    age: {
        type: String,
        default: null
    },
    waters: {
        type: String,
        default: null
    },
    multiplication: {
        type: String,
        default: null
    },
    characteristics: {
        type: String,
        default: null
    },
    attitude: {
        type: String,
        default: null
    },
    food: {
        type: String,
        default: null
    },
    aquariumRegion: [{
        type: String,
        default: null
    }],
    liter: {
        type: String,
        default: null
    },
    edgeLength: {
        type: String,
        default: null
    },
    description: {
        type: String,
        default: null
    },
    synonyms: {
        type: String,
        default: null
    },
    discoverer: {
        type: String,
        default: null
    },
    discovererYear: {
        type: String,
        default: null
    },
    previewImage: {
        type: String,
        default: null
    },
    images: [{
        type: String,
        default: null
    }]
});
fishSchema.plugin(autoIncrement, {id:'fish_seq',inc_field: '_id'});
module.exports = mongoose.model('Fish', fishSchema);
